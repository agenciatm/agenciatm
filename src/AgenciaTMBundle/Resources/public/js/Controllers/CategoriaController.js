
var CategoriaController = (function () {

    /**
     * metedo que sirve para armar un objeto categoria que sera enviado al guardar o editar
     * @returns {CategoriaController.armarCategoria.categoria|Object}
     */
    var armarCategoria = function () {
        var categoria = new Object();
        categoria.id = $("#idCategoria").val();
        categoria.nombre = $("#nombre").val();
        categoria.descripcion = $("#descripcion").val();
        return categoria;
    }

    /**
     * metedo que se ejecuta luego de la llamada ajax para guardar o editar una categoria
     * @param {type} data
     * @returns {undefined}
     */
    var guardarEditarCategoriaDone = function (data) {
        if (data.status === 'Ok') {
            setPNotify('OperacionExitosa', data.msg, 'success');
            if (data.code == "201") {
                resetForm(".formCategoria")
            }
        } else {
            showErrorsServer(data);
        }
    }

    /**
     * metedo que se ejecuta luego de la llamada ajax de eliminar categoria
     * @param {type} data
     * @returns {undefined}
     */
    var eliminarCategoriaDone = function (data) {
        if (data.status === "Ok") {
            $("#tr-" + data.id).remove();
//            $("#tr-" + data.id).remove();
            setPNotify('Operacion Exitosa', data.msg, 'success');
        } else {
            setPNotify('Error', data.msg, 'error');
        }
    }

    return {
        eliminarCategoria: function () {
            $(document).on("click", '.categoriaEliminar', function () {
                var name = $(this).data('nombre');
                var path = $(this).data('url');
                bootbox.confirm("¿Desea seguro eliminar la categoria " + name.toUpperCase() + "?", function (result) {
                    if (result) {
                        $.ajax({
                            type: "DELETE",
                            url: path,
                        }).done(eliminarCategoriaDone);
                    }
                });
            })
        },
        //llamada ajax para guardar o editar una categiria
        guardarEditarCategoria: function () {
            $(".formCategoria").submit(function (e) {
                e.preventDefault();
                if (validator.checkAll($(".formCategoria"))) {
                    var path = $('.formCategoria').attr('action');
                    var method = $('.formCategoria').attr('method');
                    var categoria = armarCategoria();
                    $.ajax({
                        type: method,
                        url: path,
                        data: categoria,
                        dataType: 'json'
                    }).done(guardarEditarCategoriaDone)
                }
            })
        },

        init: function () {
            this.eliminarCategoria();
        },
        initNewEdit: function () {
            initValidation(".formCategoria");
            this.guardarEditarCategoria();
        },
    }
});



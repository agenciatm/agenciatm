
var UsuarioController = (function () {

    /**
     * metedo que sirve para armar un objeto usuario que sera enviado al guardar o editar
     * @returns {UsuarioController.armarUsuario.usuario|Object}
     */
    var armarUsuario = function () {
        var usuario = new Object();
        usuario.id = $("#idUsuario").val();
        usuario.nombre = $("#nombre").val();
        usuario.apellido = $("#apellido").val();
        usuario.dni = $("#dni").val();
        usuario.email = $("#email").val();
        usuario.domicilio = $("#domicilio").val();
        usuario.password = $("#password").val();
        usuario.rol = $("#rol").val();
        return usuario;
    }

    /**
     * metedo que se ejecuta luego de la llamada ajax para guardar o editar una usuario
     * @param {type} data
     * @returns {undefined}
     */
    var guardarEditarUsuarioDone = function (data) {
        if (data.status === 'Ok') {
            setPNotify('OperacionExitosa', data.msg, 'success');
            if (data.code == "201") {
                resetForm(".formUsuario")
            }
        } else {
            showErrorsServer(data);
        }
    }

    /**
     * metedo que se ejecuta luego de la llamada ajax de eliminar usuario
     * @param {type} data
     * @returns {undefined}
     */
    var eliminarUsuarioDone = function (data) {
        if (data.status === "Ok") {
            $("#tr-" + data.id).remove();
//            $("#tr-" + data.id).remove();
            setPNotify('Operacion Exitosa', data.msg, 'success');
        } else {
            setPNotify('Error', data.msg, 'error');
        }
    }

    return {
        eliminarUsuario: function () {
            $(document).on("click", '.usuarioEliminar', function () {
                var name = $(this).data('nombre');
                var path = $(this).data('url');
                bootbox.confirm("¿Desea seguro eliminar la usuario " + name.toUpperCase() + "?", function (result) {
                    if (result) {
                        $.ajax({
                            type: "DELETE",
                            url: path,
                        }).done(eliminarUsuarioDone);
                    }
                });
            })
        },
        //llamada ajax para guardar o editar una usuario
        guardarEditarUsuario: function () {
            $(".formUsuario").submit(function (e) {
                e.preventDefault();
                if (validator.checkAll($(".formUsuario"))) {
                    var path = $('.formUsuario').attr('action');
                    var method = $('.formUsuario').attr('method');
                    var usuario = armarUsuario();
                    $.ajax({
                        type: method,
                        url: path,
                        data: usuario,
                        dataType: 'json'
                    }).done(guardarEditarUsuarioDone)
                }
            })
        },

        init: function () {
            this.eliminarUsuario();
        },
        initNewEdit: function () {
            initValidation(".formUsuario");
            this.guardarEditarUsuario();
        },
    }
});



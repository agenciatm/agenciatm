var baseUrl = "http:\\localhost:8000/";

/**
 * metodo para inicializar la tabla de barrios
 * @returns {undefined}
 */
function initTableSearch() {
    $(".resultadosTable").DataTable({
        ordering: true,
        lengthChange: false,
        destroy: true,
        "iDisplayLength": 15,
        "bInfo": false,
        "language": {
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }
    })
}

function actualizarTabla(id) {
    $("#" + id + " tbody").remove();
    $("#" + id).append("<tbody></tbody>");
}

function ocultarMostrar(id1, id2) {
    $("#" + id1).removeClass("show");
    $("#" + id1).addClass("hide");
    $("#" + id2).removeClass("hide");
    $("#" + id2).addClass("show");
}

function ocultar(id) {
    $("#" + id).removeClass("show");
    $("#" + id).addClass("hide");
}

function mostrar(id) {
    $("#" + id).removeClass("hide");
    $("#" + id).addClass("show");
}

function getFormatDate(d) {
    return  d.getDate() + 1 + '/' + d.getMonth() + '/' + d.getFullYear()
}

/**
 * metodo para convertir fecha timestamp en una cadena de fecha
 * @param {type} timestamp
 * @returns {String}
 * 
 */
function parseTimestapToString(timestamp) {
    var f = new Date(timestamp * 1000);
    var dia = f.getDate();
    if (f.getDate() < 10) {
        dia = "0" + f.getDate();
    }
    var mes = f.getMonth() + 1;
    if (mes < 10) {
        mes = "0" + (f.getMonth() + 1);
    }
    var hora = f.getHours();
    if (hora < 10) {
        hora = "0" + f.getHours();
    }
    var minutos = f.getMinutes();
    if (minutos < 10) {
        minutos = "0" + f.getMinutes();
    }
    var fecha = dia + "-" + mes + "-" + f.getFullYear() + " " + hora + ":" + minutos + ":" + f.getSeconds();
    return fecha;
}

/**
 * Metodo para resetear los datos de un formulario
 * @param {type} form
 * @returns {undefined}
 */
function resetForm(form) {
    $(form).each(function () {
        this.reset();
    });
}

/**
 * metodo para sertear las notificaciones de alertas
 * @param {type} title
 * @param {type} msg
 * @param {type} type
 * @returns {undefined}
 */
function setPNotify(title, msg, type) {
    new PNotify({
        title: title,
        text: msg,
        type: type,
        styling: 'bootstrap3'
    });
}

/**
 * metodo para mostrar los errores del servidor
 * @param {type} data
 * @returns {undefined}
 */
function showErrorsServer(data){
    var errors = JSON.parse(data.data);
    var msg = data.msg;
    for(var error in errors){
        msg = msg +"<br>"+ errors[error];
    }
    setPNotify('Errores Encontrados',msg,'error');
}

/**
 * Metodo para iniciar validaciones, se le pasa el id o clase del formulario en cuestion
 * @param {type} form
 * @returns {undefined}
 */
function initValidation(form){
     
      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $(form)
        .on('blur', 'textarea, input[required], input.optional, select.required', validator.checkField)
        .on('change', ' select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

}
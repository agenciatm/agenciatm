<?php

namespace CMVidaBundle\Resources\config;

/**
 * Description of Constantes
 *
 */
class Constantes {

    // TIPOS DE ENTIDADES
    const TYPE_SERMON_DIARIO = 1;
    const TYPE_SERMON_SEMANAL = 2;
    const TYPE_NOTICIA = 3;
    
    const URL_SERVER = "http://localhost:8000/";
    // INSTANCIAS
    const INSTANCIA_CAPITAL = 1;
    const INSTANCIA_PROVINCIAL = 2;
    const INSTANCIA_NACIONAL = 3;
    // TIPOS CATEGORIAS
    const CATEGORIA_CARROZA = 1;
    const CATEGORIA_CARROZA_TECNICA = 2;
    const CATEGORIA_CARRUAJE = 3;
    // TIPOS CATEGORIAS ELECCION
    const REINA = "reina";
    const PRIM_PRINCESA = "primPrincesa";
    const SEG_PRINCESA = "segPrincesa";
    const PRIM_DAMA = "primDama";
    const SEG_DAMA = "segDama";

    #DESFILE DE CARROZAS
    const DESFILE_A = 1;
    const DESFILE_B = 2;
    const DESFILE_AB1 = 3;
    const DESFILE_AB2 = 4;
    const DESFILE_AB3 = 5;
    
    #OTROS
    const TOTAL_JURADO = 5;
    const VALIDATION_FAILED = "Validation Failed";
    
    #NOMBRES CATEGORIAS CARROZAS
    const NOM_CAT_CARROZA= "carroza";
    const NOM_CAT_CARROZA_TECNICA= "carrozaTecnica";
    const NOM_CAT_CARRUAJE= "carruaje";
    
    #ERROR
    const ERROR_PODIO = "El podio elegido no coincide con el podio de la instancia.";
    const ERROR_USUARIO_MAC = "Se especifica jurado y mac.";    
    const ERROR_VOTO_REPETIDO = "Ud ya registro su voto para esta instancia.";    
}

<?php

namespace AgenciaTMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FotografiaType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('titulo')
                ->add('epigrafe')
                ->add('rutaOriginal')
                ->add('rutaMarca')
                ->add('fotografiasTags')
                ->add('categoria')
                ->add('usuario')
                ->add('galeria');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AgenciaTMBundle\Entity\Fotografia',
            'csrf_protection' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'agenciatmbundle_fotografia';
    }

}

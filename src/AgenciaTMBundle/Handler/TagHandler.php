<?php

namespace AgenciaTMBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use AgenciaTMBundle\HandlerInterface\TagHandlerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use AgenciaTMBundle\Model\TagInterface;
use AgenciaTMBundle\Form\TagType;
use AgenciaTMBundle\Exception\InvalidFormException;

/**
 * Description of TagHandler
 *
 */
class TagHandler implements TagHandlerInterface {

    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

//    private $securityContext;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
//        $this->securityContext = $securityContext;
    }

    /**
     * Devuelve un tag.
     *
     * @param mixed $id
     *
     * @return TagInterface
     */
    public function get($id) {
        return $this->repository->find($id);
    }

    /**
     * Devuelve una lista de tags.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all() {
        return $this->repository->findBy(array(), array('name' => 'ASC'));
    }

    /**
     * Crea un nuevo tag.
     *
     * @param array $parameters
     *
     * @return TagInterface
     */
    public function post(array $parameters) {
        $tag = $this->createTag();
        return $this->processForm($tag, $parameters, 'POST');
    }

    /**
     * Edita un tag.
     *
     * @param TagInterface $tag
     * @param array         $parameters
     *
     * @return TagInterface
     */
    public function put(TagInterface $tag, array $parameters) {
        return $this->processForm($tag, $parameters, 'PUT');
    }

    /**
     * Actualiza parcialmente un tag.
     *
     * @param TagInterface $tag
     * @param array         $parameters
     *
     * @return TagInterface
     */
    public function patch(TagInterface $tag, array $parameters) {
        return $this->processForm($tag, $parameters, 'PATCH');
    }

    /**
     * Elimina un tag.
     *
     * @param TagInterface $tag
     * @param array         $parameters
     *
     * @return TagInterface
     */
    public function delete(TagInterface $tag) {
        $this->om->remove($tag);
        $this->om->flush($tag);
        return $tag;
    }

    /**
     * Processes the form.
     *
     * @param TagInterface $tag
     * @param array         $parameters
     * @param String        $method
     *
     * @return TagInterface
     *
     * @throws \Cilo\DenunciaBundle\Exception\InvalidFormException
     */
    private function processForm(TagInterface $tag, array $parameters, $method = "PUT") {
        $form = $this->formFactory->create(new TagType(), $tag, array('method' => $method));
        $form->submit($parameters);
        if ($form->isValid()) {
            $tag = $form->getData();
            if ($method == "POST") {
                $tag->setCreatedAt(new \DateTime());
            } else {
                $tag->setUpdatedAt(new \DateTime());
            }
            $this->om->persist($tag);
            $this->om->flush($tag);
            return $tag;
        }
        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createTag() {
        return new $this->entityClass();
    }

    /**
     * Devuelve tags para la busqueda de autocompletar.
     *
     * @param mixed $query
     *
     * @return TagInterface
     */
    public function autocomplete($query) {
        return $this->repository->findAutocomplete($query);
    }

}

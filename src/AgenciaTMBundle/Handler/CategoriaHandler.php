<?php

namespace AgenciaTMBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use AgenciaTMBundle\HandlerInterface\CategoriaHandlerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use AgenciaTMBundle\Model\CategoriaInterface;
use AgenciaTMBundle\Form\CategoriaType;
use \Doctrine\ORM\ORMException;

//use AgenciaTMBundle\Exception\InvalidFormException;

/**
 * Description of CategoriaHandler
 *
 */
class CategoriaHandler implements CategoriaHandlerInterface {

    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;
    private $container;

//    private $securityContext;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory, ContainerInterface $container) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
        $this->container = $container;
//        $this->securityContext = $securityContext;
    }

    /**
     * Devuelve un categoria.
     *
     * @param mixed $id
     *
     * @return CategoriaInterface
     */
    public function get($id) {
        return $this->repository->find($id);
    }

    /**
     * Devuelve una lista de categorias.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all() {
        return $this->repository->findBy(array(), array('nombre' => 'ASC'));
    }

    /**
     * Crea un nuevo categoria.
     *
     * @param array $parameters
     *
     * @return CategoriaInterface
     */
    public function post(array $parameters) {
        $categoria = $this->createCategoria();
        return $this->processForm($categoria, $parameters, 'POST');
    }

    /**
     * Edita un categoria.
     *
     * @param CategoriaInterface $categoria
     * @param array         $parameters
     *
     * @return CategoriaInterface
     */
    public function put(CategoriaInterface $categoria, array $parameters) {
        return $this->processForm($categoria, $parameters, 'PUT');
    }

    /**
     * Actualiza parcialmente un categoria.
     *
     * @param CategoriaInterface $categoria
     * @param array         $parameters
     *
     * @return CategoriaInterface
     */
    public function patch(CategoriaInterface $categoria, array $parameters) {
        return $this->processForm($categoria, $parameters, 'PATCH');
    }

    /**
     * Elimina un categoria.
     *
     * @param CategoriaInterface $categoria
     * @param array         $parameters
     *
     * @return CategoriaInterface
     */
    public function delete(CategoriaInterface $categoria) {
        try {
            $this->om->remove($categoria);
            $this->om->flush($categoria);
            return $categoria;
        } catch (Exception $ex) {
            // TOMAR EL ERROR EN CASO DE Q NO SE PUEDA ELIMINAR VER.
        }
    }

    /**
     * Processes the form.
     *
     * @param CategoriaInterface $categoria
     * @param array         $parameters
     * @param String        $method
     *
     * @return CategoriaInterface
     *
     */
    private function processForm(CategoriaInterface $categoria, array $parameters, $method = "PUT") {
        $form = $this->formFactory->create(new CategoriaType(), $categoria, array('method' => $method));
        $form->submit($parameters);
        if ($form->isValid()) {
            $categoria = $form->getData();
            if ($method == "POST") {
                $categoria->setCreatedAt(new \DateTime());
            } else {
                $categoria->setUpdatedAt(new \DateTime());
            }
            $this->om->persist($categoria);
            $this->om->flush($categoria);
            return $categoria;
        }
        $error = $this->container->get('agenciatm.formerrors.handler')->getArray($form);
        return $error;
//        return $form->getErrors();
//        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createCategoria() {
        return new $this->entityClass();
    }

    /**
     * Devuelve categorias para la busqueda de autocompletar. NO SE USA AUN
     *
     * @param mixed $query
     *
     * @return CategoriaInterface
     */
    public function autocomplete($query) {
        return $this->repository->findAutocomplete($query);
    }

}

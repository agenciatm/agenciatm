<?php

namespace AgenciaTMBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use AgenciaTMBundle\HandlerInterface\UsuarioHandlerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use AgenciaTMBundle\Model\UsuarioInterface;
use AgenciaTMBundle\Form\UsuarioType;
use AgenciaTMBundle\Exception\InvalidFormException;
use \Doctrine\ORM\ORMException;

/**
 * Description of UsuarioHandler
 *
 */
class UsuarioHandler implements UsuarioHandlerInterface {

    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;
    private $securityContext;
    private $container;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory, SecurityContextInterface $securityContext, ContainerInterface $container) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
        $this->securityContext = $securityContext;
        $this->container = $container;
    }

    /**
     * Devuelve un usuario.
     *
     * @param mixed $id
     *
     * @return UsuarioInterface
     */
    public function get($id) {
        return $this->repository->find($id);
    }

    /**
     * Devuelve una lista de usuarios.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all() {
        return $this->repository->findBy(array(), array('apellido' => 'ASC'));
    }

    /**
     * Crea un nuevo usuario.
     *
     * @param array $parameters
     *
     * @return UsuarioInterface
     */
    public function post(array $parameters) {
        $usuario = $this->createUsuario();
        return $this->processForm($usuario, $parameters, 'POST');
    }

    /**
     * Edita un usuario.
     *
     * @param UsuarioInterface $usuario
     * @param array         $parameters
     *
     * @return UsuarioInterface
     */
    public function put(UsuarioInterface $usuario, array $parameters) {
        return $this->processForm($usuario, $parameters, 'PUT');
    }

    /**
     * Actualiza parcialmente un usuario.
     *
     * @param UsuarioInterface $usuario
     * @param array         $parameters
     *
     * @return UsuarioInterface
     */
    public function patch(UsuarioInterface $usuario, array $parameters) {
        return $this->processForm($usuario, $parameters, 'PATCH');
    }

    /**
     * Elimina un usuario.
     *
     * @param UsuarioInterface $usuario
     * @param array         $parameters
     *
     * @return UsuarioInterface
     */
    public function delete(UsuarioInterface $usuario) {
        try {
            $this->om->remove($usuario);
            $this->om->flush($usuario);
            return $usuario;
        } catch (Exception $ex) {
            
        }
    }

    /**
     * Processes the form.
     *
     * @param UsuarioInterface $usuario
     * @param array         $parameters
     * @param String        $method
     *
     * @return UsuarioInterface
     *
     * @throws \Cilo\DenunciaBundle\Exception\InvalidFormException
     */
    private function processForm(UsuarioInterface $usuario, array $parameters, $method = "PUT") {
        $current_pass = $usuario->getPassword();
        $form = $this->formFactory->create(new UsuarioType(), $usuario, array('method' => $method));
        $form->submit($parameters);
        if ($form->isValid()) {
            $usuario = $form->getData();
            if ($method == "POST") {
                $this->setSecurePassword($usuario);
                $usuario->setCreatedAt(new \DateTime());
            } else {
                if ($usuario->getPassword() != '') {
                    $this->setSecurePassword($usuario);
                } else {
                    $usuario->setPassword($current_pass);
                }
                $usuario->setUpdatedAt(new \DateTime());
            }
            $this->om->persist($usuario);
            $this->om->flush($usuario);
            return $usuario;
        }
        $error = $this->container->get('agenciatm.formerrors.handler')->getArray($form);
        return $error;
    }

    private function createUsuario() {
        return new $this->entityClass();
    }

    /**
     * Metodo para generar la contraseña de usuario
     * @param type $entity
     */
    private function setSecurePassword(&$entity) {
        $entity->setSalt(md5(time()));
        $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /**
     * Devuelve el usuario logeado.
     *     
     *
     * @return UsuarioInterface
     */
    public function getUsuarioLogeado() {
        $userLogin = $this->securityContext->getToken()->getUsuario();
        if (!$userLogin) {
            throw new Exception("No se ha logeado ningun usuario ", "500");
        }
        return $userLogin;
    }

    public function getUserLogeado() {
        
    }

}

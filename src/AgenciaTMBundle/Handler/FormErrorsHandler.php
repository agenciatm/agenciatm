<?php

namespace AgenciaTMBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use AgenciaTMBundle\HandlerInterface\FormErrorsHandlerInterface;

/**
 * Description of TagHandler
 *
 */
class FormErrorsHandler implements FormErrorsHandlerInterface {

    /**
     * metodo que devuelve los errores de un formulario como array 
     * @param \Symfony\Component\Form\Form $form
     * @return type
     */
    public function getArray(\Symfony\Component\Form\Form $form) {
        return $this->getErrors($form);
    }

    private function getErrors($form) {
        $errors = array();

        if ($form instanceof \Symfony\Component\Form\Form) {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }

            foreach ($form->all() as $key => $child) {
                /** @var $child \Symfony\Component\Form\Form */
                if ($err = $this->getErrors($child)) {
                    $errors[$key] = $err;
                }
            }
        }
        return $errors;
    }

}

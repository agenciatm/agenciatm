<?php

namespace AgenciaTMBundle\HandlerInterface;

use AgenciaTMBundle\Model\UsuarioInterface;

interface UsuarioHandlerInterface {

    /**
     * Devuelve un usuario de acuerdo al identificador
     *
     * @api
     *
     * @param mixed $id
     *
     * @return UsuarioInterface
     */
    public function get($id);

    /**
     * Devuelve la lista de usuarios.
     *
     * @param int $limit 
     * @param int $offset 
     *
     * @return array
     */
    public function all();

    /**
     * Crea un nuevo usuario.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return UsuarioInterface
     */
    public function post(array $parameters);

    /**
     * Edita un usuario.
     *
     * @api
     *
     * @param UsuarioInterface   $usuario
     * @param array           $parameters
     *
     * @return UsuarioInterface
     */
    public function put(UsuarioInterface $usuario, array $parameters);

    /**
     * Actualiza parcialmente un usuario.
     *
     * @api
     *
     * @param UsuarioInterface   $usuario
     * @param array           $parameters
     *
     * @return UsuarioInterface
     */
    public function patch(UsuarioInterface $usuario, array $parameters);

    /**
     * Elimina un usuario.
     *
     * @api
     *
     * @param UsuarioInterface $usuario
     *
     * @return UsuarioInterface
     */
    public function delete(UsuarioInterface $usuario);

    /**
     * Devuelve el usuario logeado.
     *     
     *
     * @return UsuarioInterface
     */
    public function getUserLogeado();
    
}

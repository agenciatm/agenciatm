<?php

namespace AgenciaTMBundle\HandlerInterface;


interface FormErrorsHandlerInterface {

    /**
     * Devuelve los errores de un formulario como array
     *
     * @api
     *
     * @param mixed $form
     *
     * @return TagInterface
     */
    public function getArray(\Symfony\Component\Form\Form $form);
}

<?php

namespace AgenciaTMBundle\HandlerInterface;

use AgenciaTMBundle\Model\CategoriaInterface;

interface CategoriaHandlerInterface {

    /**
     * Devuelve un categoria de acuerdo al identificador
     *
     * @api
     *
     * @param mixed $id
     *
     * @return CategoriaInterface
     */
    public function get($id);

    /**
     * Devuelve la lista de categorias.
     *
     * @param int $limit 
     * @param int $offset 
     *
     * @return array
     */
    public function all();

    /**
     * Crea un nuevo categoria.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return CategoriaInterface
     */
    public function post(array $parameters);

    /**
     * Edita un categoria.
     *
     * @api
     *
     * @param CategoriaInterface   $categoria
     * @param array           $parameters
     *
     * @return CategoriaInterface
     */
    public function put(CategoriaInterface $categoria, array $parameters);

    /**
     * Actualiza parcialmente un categoria.
     *
     * @api
     *
     * @param CategoriaInterface   $categoria
     * @param array           $parameters
     *
     * @return CategoriaInterface
     */
    public function patch(CategoriaInterface $categoria, array $parameters);

    /**
     * Elimina un categoria.
     *
     * @api
     *
     * @param CategoriaInterface $categoria
     *
     * @return CategoriaInterface
     */
    public function delete(CategoriaInterface $categoria);

    /**
     * Devuelve categorias para busqueda de autocompeltar
     *
     * @api
     *
     * @param mixed $query
     *
     * @return CategoriaInterface
     */
    public function autocomplete($query);
}

<?php

namespace AgenciaTMBundle\HandlerInterface;

use AgenciaTMBundle\Model\TagInterface;

interface TagHandlerInterface {

    /**
     * Devuelve un tag de acuerdo al identificador
     *
     * @api
     *
     * @param mixed $id
     *
     * @return TagInterface
     */
    public function get($id);

    /**
     * Devuelve la lista de tags.
     *
     * @param int $limit 
     * @param int $offset 
     *
     * @return array
     */
    public function all();

    /**
     * Crea un nuevo tag.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return TagInterface
     */
    public function post(array $parameters);

    /**
     * Edita un tag.
     *
     * @api
     *
     * @param TagInterface   $tag
     * @param array           $parameters
     *
     * @return TagInterface
     */
    public function put(TagInterface $tag, array $parameters);

    /**
     * Actualiza parcialmente un tag.
     *
     * @api
     *
     * @param TagInterface   $tag
     * @param array           $parameters
     *
     * @return TagInterface
     */
    public function patch(TagInterface $tag, array $parameters);

    /**
     * Elimina un tag.
     *
     * @api
     *
     * @param TagInterface $tag
     *
     * @return TagInterface
     */
    public function delete(TagInterface $tag);

    /**
     * Devuelve tags para busqueda de autocompeltar
     *
     * @api
     *
     * @param mixed $query
     *
     * @return TagInterface
     */
    public function autocomplete($query);
}

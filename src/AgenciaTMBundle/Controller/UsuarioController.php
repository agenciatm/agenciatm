<?php

namespace AgenciaTMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormTypeInterface;
use AgenciaTMBundle\Exception\InvalidFormException;
use AgenciaTMBundle\Form\UsuarioType;
use AgenciaTMBundle\Model\UsuarioInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AgenciaTMBundle\Entity\Usuario;

/**
 * Description of UsuarioController
 * @Route("/usuario")
 */
class UsuarioController extends Controller {

    /**
     * Lists all Usuarios entities. LLAMADA A LA VISTA DE LISTA DE CATEGORIAS
     *
     * @Route("/", name="usuario_index")
     * @Method("GET")
     */
    public function indexAction(Request $request) {
        $usuarios = $this->container->get('agenciatm.usuario.handler')->all();
        $paginator = $this->get('knp_paginator');
        // se llama a la componene de paginacion
        $pagination = $paginator->paginate(
                $usuarios, /* query */ $request->query->getInt('page', 1), /* page number */ 10 /* limit per page */
        );
        return $this->render('usuario/index.html.twig', array(
                    'usuarios' => $pagination,
        ));
    }

    /**
     * Creates a new Usuario entity. LLAMADA A LA VISTA DE NUEVO
     *
     * @Route("/new", name="usuario_new")
     * @Method({"GET"})
     */
    public function newAction(Request $request) {
        $usuario = new Usuario();
        $roles = $this->container->get('agenciatm.rol.handler')->all();
        return $this->render('usuario/new.html.twig', array(
                    'usuario' => $usuario,
                    'roles' => $roles
        ));
    }

    /**
     * Creates a new Empresa entity. METODO PARA GUARDAR CATEGORIA
     *
     * @Route("/new", name="usuario_create")
     * @Method({"POST"})
     */
    public function createAction(Request $request) {

        try {
            $data = $this->container->get('agenciatm.usuario.handler')->post(
                    $request->request->all()
            );
            if ($data instanceof Usuario) {
                $result = $this->setResponse('Ok', Response::HTTP_CREATED, 'Se creó al usuario exitosamente.', $data);
            } else {
                $result = $this->setResponse('Error', Response::HTTP_INTERNAL_SERVER_ERROR, 'Error, no se pudo crear la usuario.', json_encode($data));
            }
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Creates a new Empresa entity. METODO PARA EDITAR CATEGORIA CON PUT
     *
     * @Route("/edit/{id}", name="usuario_editar_put")
     * @Method({"PUT"})
     */
    public function editPutAction(Request $request, $id) {
        try {

            $data = $this->container->get('agenciatm.usuario.handler')->put(
                    $this->getOr404($id), $request->request->all()
            );
            if ($data instanceof Usuario) {
                $result = $this->setResponse('Ok', Response::HTTP_OK, 'Se editó al usuario exitosamente.', $data);
            } else {
                $result = $this->setResponse('Error', Response::HTTP_INTERNAL_SERVER_ERROR, 'Error, no se pudo editar la usuario.', json_encode($data));
            }
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Creates a new Empresa entity. METODO PARA EDITAR CATEGORIA CON PATH
     *
     * @Route("/edit/{id}", name="usuario_editar_patch")
     * @Method({"PATCH"})
     */
    public function editPatchAction(Request $request) {
        try {
            $data = $this->container->get('agenciatm.usuario.handler')->patch(
                    $this->getOr404($id), $request->request->all()
            );
            if ($data instanceof Usuario) {
                $result = $this->setResponse('Ok', Response::HTTP_OK, 'Se editó al usuario exitosamente.', $data);
            } else {
                $result = $this->setResponse('Error', Response::HTTP_INTERNAL_SERVER_ERROR, 'Error, no se pudo editar la usuario.', json_encode($data));
            }
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Finds and displays a Empresa entity. LLAMADA A LA VISTA DE SHOW
     *
     * @Route("/{id}", name="usuario_show")
     * @Method("GET")
     */
    public function showAction($id) {
        $usuario = $this->getOr404($id);
        return $this->render('usuario/show.html.twig', array(
                    'usuario' => $usuario,
        ));
    }

    /**
     * Displays a form to edit an existing Empresa entity. LLAMADA A LA VISTA DE EDITAR
     *
     * @Route("/{id}/edit", name="usuario_edit")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $id) {
        $usuario = $this->getOr404($id);
        $roles = $this->container->get('agenciatm.rol.handler')->all();
        return $this->render('usuario/edit.html.twig', array(
                    'usuario' => $usuario,
                    'roles' => $roles
        ));
    }

    /**
     * Deletes a Empresa entity. METODO PARA ELIMINAR UNA CATEGORIA
     *
     * @Route("/{id}", name="usuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        if ($request->isXmlHttpRequest()) {  // si es una peticion AJAX
            $usuario = $this->container->get('agenciatm.usuario.handler')->delete($this->getOr404($id));
            // TODO COMPROBOAR EL ERROR
            $result = $this->setResponse('Ok', Response::HTTP_OK, 'El usuario fue eliminado con exito.', $id);
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * Devuelve el usuario o un 404 Exception.
     *
     * @param mixed $id
     *
     * @return UsuarioInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id) {
        if (!($usuario = $this->container->get('agenciatm.usuario.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('El elemento \'%s\' no se encontró.', $id));
        }
        return $usuario;
    }

    /**
     * Metodo para formar la respuesta del servidor
     * @param type $status
     * @param type $code
     * @param type $msg
     * @param type $data
     * @return type
     */
    protected function setResponse($status, $code, $msg, $data) {
        $result = array(
            'status' => $status,
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        return $result;
    }

}

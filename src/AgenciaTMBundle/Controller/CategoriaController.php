<?php

namespace AgenciaTMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormTypeInterface;
use AgenciaTMBundle\Exception\InvalidFormException;
use AgenciaTMBundle\Form\CategoriaType;
use AgenciaTMBundle\Model\CategoriaInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AgenciaTMBundle\Entity\Categoria;

/**
 * Description of CategoriaController
 * @Route("/categoria")
 */
class CategoriaController extends Controller {

    /**
     * Lists all Categorias entities. LLAMADA A LA VISTA DE LISTA DE CATEGORIAS
     *
     * @Route("/", name="categoria_index")
     * @Method("GET")
     */
    public function indexAction(Request $request) {
        $categorias = $this->container->get('agenciatm.categoria.handler')->all();
        $paginator = $this->get('knp_paginator');
        // se llama a la componene de paginacion
        $pagination = $paginator->paginate(
                $categorias, /* query */ $request->query->getInt('page', 1), /* page number */ 10 /* limit per page */
        );
        return $this->render('categoria/index.html.twig', array(
                    'categorias' => $pagination,
        ));
    }

    /**
     * Creates a new Empresa entity. LLAMADA A LA VISTA DE NUEVO
     *
     * @Route("/new", name="categoria_new")
     * @Method({"GET"})
     */
    public function newAction(Request $request) {
        $categoria = new Categoria();
        return $this->render('categoria/new.html.twig', array(
                    'categoria' => $categoria,
        ));
    }

    /**
     * Creates a new Empresa entity. METODO PARA GUARDAR CATEGORIA
     *
     * @Route("/new", name="categoria_create")
     * @Method({"POST"})
     */
    public function createAction(Request $request) {

        try {
            $data = $this->container->get('agenciatm.categoria.handler')->post(
                    $request->request->all()
            );
            if ($data instanceof Categoria) {
                $result = $this->setResponse('Ok', Response::HTTP_CREATED, 'Se creó al categoria exitosamente.', $data);
            } else {
                $result = $this->setResponse('Error', Response::HTTP_INTERNAL_SERVER_ERROR, 'Error, no se pudo crear la categoria.', json_encode($data));
            }
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Creates a new Empresa entity. METODO PARA EDITAR CATEGORIA CON PUT
     *
     * @Route("/edit/{id}", name="categoria_editar_put")
     * @Method({"PUT"})
     */
    public function editPutAction(Request $request, $id) {
        try {

            $data = $this->container->get('agenciatm.categoria.handler')->put(
                    $this->getOr404($id), $request->request->all()
            );
            if ($data instanceof Categoria) {
                $result = $this->setResponse('Ok', Response::HTTP_OK, 'Se editó al categoria exitosamente.', $data);
            } else {
                $result = $this->setResponse('Error', Response::HTTP_INTERNAL_SERVER_ERROR, 'Error, no se pudo editar la categoria.', json_encode($data));
            }       
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Creates a new Empresa entity. METODO PARA EDITAR CATEGORIA CON PATH
     *
     * @Route("/edit/{id}", name="categoria_editar_patch")
     * @Method({"PATCH"})
     */
    public function editPatchAction(Request $request) {
        try {
            $data = $this->container->get('agenciatm.categoria.handler')->patch(
                    $this->getOr404($id), $request->request->all()
            );
            if ($data instanceof Categoria) {
                $result = $this->setResponse('Ok', Response::HTTP_OK, 'Se editó al categoria exitosamente.', $data);
            } else {
                $result = $this->setResponse('Error', Response::HTTP_INTERNAL_SERVER_ERROR, 'Error, no se pudo editar la categoria.', json_encode($data));
            }        
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Finds and displays a Empresa entity. LLAMADA A LA VISTA DE SHOW
     *
     * @Route("/{id}", name="categoria_show")
     * @Method("GET")
     */
    public function showAction($id) {
        $categoria = $this->getOr404($id);
        return $this->render('categoria/show.html.twig', array(
                    'categoria' => $categoria,
        ));
    }

    /**
     * Displays a form to edit an existing Empresa entity. LLAMADA A LA VISTA DE EDITAR
     *
     * @Route("/{id}/edit", name="categoria_edit")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $id) {
        $categoria = $this->getOr404($id);
        return $this->render('categoria/edit.html.twig', array(
                    'categoria' => $categoria,
        ));
    }

    /**
     * Deletes a Empresa entity. METODO PARA ELIMINAR UNA CATEGORIA
     *
     * @Route("/{id}", name="categoria_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        if ($request->isXmlHttpRequest()) {  // si es una peticion AJAX
            $categoria = $this->container->get('agenciatm.categoria.handler')->delete($this->getOr404($id));
            // TODO COMPROBAR POR EL ERROR
            $result = $this->setResponse('Ok', Response::HTTP_OK, 'La categoria fue eliminada con exito.', $id);
            
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * Devuelve el categoria o un 404 Exception.
     *
     * @param mixed $id
     *
     * @return UsuarioInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id) {
        if (!($categoria = $this->container->get('agenciatm.categoria.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('El elemento \'%s\' no se encontró.', $id));
        }
        return $categoria;
    }

    /**
     * Metodo para formar la respuesta del servidor
     * @param type $status
     * @param type $code
     * @param type $msg
     * @param type $data
     * @return type
     */
    protected function setResponse($status, $code, $msg, $data) {
        $result = array(
            'status' => $status,
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        return $result;
    }

}

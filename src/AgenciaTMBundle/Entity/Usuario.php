<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AgenciaTMBundle\Model\UsuarioInterface;
use Symfony\Component\Validator\Constraints as Assert;
//use JMS\Serializer\Annotation\ExclusionPolicy;
//use JMS\Serializer\Annotation\Expose;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\UsuarioRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="El email debe ser único."
 * )
 */
class Usuario implements UserInterface, UsuarioInterface {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150)
     * @Assert\NotBlank(message = "Por favor, ingrese Nombre")
     * @Assert\Length(
     *      min = 3,
     *      max = 100,     
     *      minMessage = "El nombre por lo menos debe tener {{ limit }} caracteres",
     *      maxMessage = "El nombre no debe tener mas de {{ limit }} caracteres"
     * )
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=100)
     * @Assert\NotBlank(message = "Por favor, ingrese Apellido")
     * @Assert\Length(
     *      min = 3,
     *      max = 100,     
     *      minMessage = "El apellido por lo menos debe tener {{ limit }} caracteres",
     *      maxMessage = "El apellido no debe tener mas de {{ limit }} caracteres"
     * )
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=8, nullable=true)
      * @Assert\Length(
     *      min = 6,
     *      max = 8,     
     *      minMessage = "El dni por lo menos debe tener {{ limit }} caracteres",
     *      maxMessage = "El dni no debe tener mas de {{ limit }} caracteres"
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="El dni {{ value }} no es numerico."
     * )
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=150, nullable=true)
     */
    private $foto;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150)
     * @Assert\NotBlank(message = "Por favor, ingrese email")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,     
     *      minMessage = "El email por lo menos debe tener {{ limit }} caracteres",
     *      maxMessage = "El email no debe tener mas de {{ limit }} caracteres"
     * )
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido."
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio", type="string", length=200, nullable=true)
     * @Assert\Length(
     *      max = 200,     
     *      maxMessage = "El domicilio no debe tener mas de {{ limit }} caracteres"
     * )
     */
    private $domicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\Regex(pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}$/i",
     *               message = "El password debe contener caracteres especiales, números, minusculas y mayusculas")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     * @Assert\NotNull(message="Debe especificar un rol.")
     */
    private $rol;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Usuario
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido() {
        return $this->apellido;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Usuario
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set domiciolio
     *
     * @param string $domicilio
     * @return Usuario
     */
    public function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domiciolio
     *
     * @return string 
     */
    public function getDomicilio() {
        return $this->domicilio;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Usuario
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Usuario
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }

    public function getRol() {
        return $this->rol;
    }

    public function setRol($rol) {
        $this->rol = $rol;
    }

    /**
     * Compares this user to another to determine if they are the same.
     *
     * @param UserInterface $user The user
     * @return boolean True if equal, false othwerwise.
     */
    public function equals(UserInterface $user) {
        return md5($this->getUsername()) == md5($user->getUsername());
    }

    public function eraseCredentials() {
        
    }

    public function getRoles() {
        return $this->getRol();
    }

    public function getUsername() {
        return $this->getEmail();
    }

    public function getFoto() {
        return $this->foto;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }

}

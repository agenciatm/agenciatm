<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AgenciaTMBundle\Model\RolInterface;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rol
 *
 * @ORM\Table(name="rol")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\RolRepository")
 */
class Rol implements RoleInterface, RolInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_alt", type="string", length=100)
     */
    private $nombreAlt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Rol
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nombreAlt
     *
     * @param string $nombreAlt
     * @return Rol
     */
    public function setNombreAlt($nombreAlt)
    {
        $this->nombreAlt = $nombreAlt;

        return $this;
    }

    /**
     * Get nombreAlt
     *
     * @return string 
     */
    public function getNombreAlt()
    {
        return $this->nombreAlt;
    }
    
    public function getRole() {
        return $this->getNombre();
    }

    public function __toString() {
        return $this->getNombreAlt();
    }
}

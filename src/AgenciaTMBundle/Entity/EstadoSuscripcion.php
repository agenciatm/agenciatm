<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AgenciaTMBundle\Model\EstadoSuscripcionInterface;

/**
 * EstadoSuscripcion
 *
 * @ORM\Table(name="estado_suscripcion")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\EstadoSuscripcionRepository")
 */
class EstadoSuscripcion implements EstadoSuscripcionInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EstadoSuscripcion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}

<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AgenciaTMBundle\Model\FotografiaInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Fotografia
 *
 * @ORM\Table(name="fotografia")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\FotografiaRepository")
 */
class Fotografia implements FotografiaInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=150)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="epigrafe", type="string", length=255, nullable=true)
     */
    private $epigrafe;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta_original", type="string", length=255)
     */
    private $rutaOriginal;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta_marca", type="string", length=255)
     */
    private $rutaMarca;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


        /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="fotografias_tags",
     *      joinColumns={@ORM\JoinColumn(name="fotografia_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $fotografiasTags;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;
    
    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Galeria")
     * @ORM\JoinColumn(name="galeria_id", referencedColumnName="id")
     */
    private $galeria;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Fotografia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set epigrafe
     *
     * @param string $epigrafe
     * @return Fotografia
     */
    public function setEpigrafe($epigrafe)
    {
        $this->epigrafe = $epigrafe;

        return $this;
    }

    /**
     * Get epigrafe
     *
     * @return string 
     */
    public function getEpigrafe()
    {
        return $this->epigrafe;
    }

    /**
     * Set rutaOriginal
     *
     * @param string $rutaOriginal
     * @return Fotografia
     */
    public function setRutaOriginal($rutaOriginal)
    {
        $this->rutaOriginal = $rutaOriginal;

        return $this;
    }

    /**
     * Get rutaOriginal
     *
     * @return string 
     */
    public function getRutaOriginal()
    {
        return $this->rutaOriginal;
    }

    /**
     * Set rutaMarca
     *
     * @param string $rutaMarca
     * @return Fotografia
     */
    public function setRutaMarca($rutaMarca)
    {
        $this->rutaMarca = $rutaMarca;

        return $this;
    }

    /**
     * Get rutaMarca
     *
     * @return string 
     */
    public function getRutaMarca()
    {
        return $this->rutaMarca;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Fotografia
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Fotografia
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    public function getFotografiasTags() {
        return $this->fotografiasTags;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getGaleria() {
        return $this->galeria;
    }

    public function setFotografiasTags($fotografiasTags) {
        $this->fotografiasTags = $fotografiasTags;
    }

    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function setGaleria($galeria) {
        $this->galeria = $galeria;
    }

}

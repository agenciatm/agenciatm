<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AgenciaTMBundle\Model\VideoInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Video
 *
 * @ORM\Table(name="video")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\VideoRepository")
 */
class Video implements VideoInterface{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=150)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="epigrafe", type="string", length=255, nullable=true, unique=true)
     */
    private $epigrafe;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=150)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_id", type="string", length=100)
     */
    private $youtubeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_id", type="datetime", nullable=true)
     */
    private $updatedId;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="videos_tags",
     *      joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $videoTags;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;
    
    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    public function __construct() {
        $this->videoTags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Video
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set epigrafe
     *
     * @param string $epigrafe
     * @return Video
     */
    public function setEpigrafe($epigrafe) {
        $this->epigrafe = $epigrafe;

        return $this;
    }

    /**
     * Get epigrafe
     *
     * @return string 
     */
    public function getEpigrafe() {
        return $this->epigrafe;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Video
     */
    public function setLink($link) {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * Set youtubeId
     *
     * @param string $youtubeId
     * @return Video
     */
    public function setYoutubeId($youtubeId) {
        $this->youtubeId = $youtubeId;

        return $this;
    }

    /**
     * Get youtubeId
     *
     * @return string 
     */
    public function getYoutubeId() {
        return $this->youtubeId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Video
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedId
     *
     * @param \DateTime $updatedId
     * @return Video
     */
    public function setUpdatedId($updatedId) {
        $this->updatedId = $updatedId;

        return $this;
    }

    /**
     * Get updatedId
     *
     * @return \DateTime 
     */
    public function getUpdatedId() {
        return $this->updatedId;
    }

    public function getVideoTags() {
        return $this->videoTags;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setVideoTags($videoTags) {
        $this->videoTags = $videoTags;
    }

    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }


}

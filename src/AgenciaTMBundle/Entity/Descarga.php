<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AgenciaTMBundle\Model\DescargaInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descarga
 *
 * @ORM\Table(name="descarga")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\DescargaRepository")
 */
class Descarga implements DescargaInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Fotografia")
     * @ORM\JoinColumn(name="fotografia_id", referencedColumnName="id")
     */
    private $fotografia;
    
    /**
     * @ORM\ManyToOne(targetEntity="Suscripcion")
     * @ORM\JoinColumn(name="suscripcion_id", referencedColumnName="id", nullable=true)
     */
    private $suscripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Compra")
     * @ORM\JoinColumn(name="compra_id", referencedColumnName="id", nullable=true)
     */
    private $compra;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Descarga
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getFotografia() {
        return $this->fotografia;
    }

    public function getSuscripcion() {
        return $this->suscripcion;
    }

    public function getCompra() {
        return $this->compra;
    }

    public function setFotografia($fotografia) {
        $this->fotografia = $fotografia;
    }

    public function setSuscripcion($suscripcion) {
        $this->suscripcion = $suscripcion;
    }

    public function setCompra($compra) {
        $this->compra = $compra;
    }

}

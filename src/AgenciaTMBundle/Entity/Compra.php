<?php

namespace AgenciaTMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AgenciaTMBundle\Model\CompraInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Compra
 *
 * @ORM\Table(name="compra")
 * @ORM\Entity(repositoryClass="AgenciaTMBundle\Repository\CompraRepository")
 */
class Compra implements CompraInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="EstadoCompra")
     * @ORM\JoinColumn(name="estado_compra_id", referencedColumnName="id")
     */
    private $estadoCompra;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Compra
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getEstadoCompra() {
        return $this->estadoCompra;
    }

    public function setEstadoCompra($estadoCompra) {
        $this->estadoCompra = $estadoCompra;
    }


}

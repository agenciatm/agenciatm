<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de galeria
 */
interface GaleriaInterface {
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Galeria
     */
    public function setNombre($nombre);

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();

    /**
     * Set epigrafe
     *
     * @param string $epigrafe
     * @return Galeria
     */
    public function setEpigrafe($epigrafe);

    /**
     * Get epigrafe
     *
     * @return string 
     */
    public function getEpigrafe();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Galeria
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Galeria
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt();
}

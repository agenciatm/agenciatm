<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de rol
 */
interface RolInterface {
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Rol
     */
    public function setNombre($nombre);

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();

    /**
     * Set nombreAlt
     *
     * @param string $nombreAlt
     * @return Rol
     */
    public function setNombreAlt($nombreAlt);

    /**
     * Get nombreAlt
     *
     * @return string 
     */
    public function getNombreAlt();
}

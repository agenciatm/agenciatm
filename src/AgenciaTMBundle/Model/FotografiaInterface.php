<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de fotografia
 */
interface FotografiaInterface {
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Fotografia
     */
    public function setTitulo($titulo);

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo();

    /**
     * Set epigrafe
     *
     * @param string $epigrafe
     * @return Fotografia
     */
    public function setEpigrafe($epigrafe);

    /**
     * Get epigrafe
     *
     * @return string 
     */
    public function getEpigrafe();

    /**
     * Set rutaOriginal
     *
     * @param string $rutaOriginal
     * @return Fotografia
     */
    public function setRutaOriginal($rutaOriginal);

    /**
     * Get rutaOriginal
     *
     * @return string 
     */
    public function getRutaOriginal();

    /**
     * Set rutaMarca
     *
     * @param string $rutaMarca
     * @return Fotografia
     */
    public function setRutaMarca($rutaMarca);

    /**
     * Get rutaMarca
     *
     * @return string 
     */
    public function getRutaMarca();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Fotografia
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Fotografia
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt();
    
    public function getFotografiasTags();

    public function getCategoria();

    public function getUsuario();

    public function getGaleria();

    public function setFotografiasTags($fotografiasTags);

    public function setCategoria($categoria);

    public function setUsuario($usuario);

    public function setGaleria($galeria);
}

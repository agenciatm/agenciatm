<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de descarga
 */
interface DescargaInterface {
    
     /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Descarga
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();
    
    public function getFotografia();

    public function getSuscripcion();

    public function getCompra();

    public function setFotografia($fotografia);

    public function setSuscripcion($suscripcion);

    public function setCompra($compra);

}

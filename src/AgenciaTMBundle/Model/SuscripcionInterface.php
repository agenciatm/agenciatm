<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de suscripcion
 */
interface SuscripcionInterface {
    
     /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return Suscripcion
     */
    public function setCantidad($cantidad);

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad();

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     * @return Suscripcion
     */
    public function setFechaDesde($fechaDesde);

    /**
     * Get fechaDesde
     *
     * @return \DateTime 
     */
    public function getFechaDesde();

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     * @return Suscripcion
     */
    public function setFechaHasta($fechaHasta);

    /**
     * Get fechaHasta
     *
     * @return \DateTime 
     */
    public function getFechaHasta();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Suscripcion
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Suscripcion
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt();
    
    public function getEstado();

    public function getTipo();

    public function getUsuario();

    public function setEstado($estado);

    public function setTipo($tipo);

    public function setUsuario($usuario);
    
}

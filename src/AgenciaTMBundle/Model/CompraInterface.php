<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de compra
 */
interface CompraInterface {
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Compra
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();
    
    public function getEstadoCompra();

    public function setEstadoCompra($estadoCompra);
}

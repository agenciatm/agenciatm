<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de tipo suscripcion
 */
interface TipoSuscripcionInterface {
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoSuscripcion
     */
    public function setNombre($nombre);
    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();
}

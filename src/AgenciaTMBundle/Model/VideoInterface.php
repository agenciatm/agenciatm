<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de rol
 */
interface VideoInterface {
    
   /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Video
     */
    public function setTitulo($titulo);

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo();

    /**
     * Set epigrafe
     *
     * @param string $epigrafe
     * @return Video
     */
    public function setEpigrafe($epigrafe);

    /**
     * Get epigrafe
     *
     * @return string 
     */
    public function getEpigrafe();

    /**
     * Set link
     *
     * @param string $link
     * @return Video
     */
    public function setLink($link);

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink();

    /**
     * Set youtubeId
     *
     * @param string $youtubeId
     * @return Video
     */
    public function setYoutubeId($youtubeId);

    /**
     * Get youtubeId
     *
     * @return string 
     */
    public function getYoutubeId();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Video
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();

    /**
     * Set updatedId
     *
     * @param \DateTime $updatedId
     * @return Video
     */
    public function setUpdatedId($updatedId);

    /**
     * Get updatedId
     *
     * @return \DateTime 
     */
    public function getUpdatedId();

    public function getVideoTags();

    public function getCategoria();

    public function getUsuario();

    public function setVideoTags($videoTags);

    public function setCategoria($categoria);

    public function setUsuario($usuario);
}

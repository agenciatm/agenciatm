<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de categoria
 */
interface CategoriaInterface {
    
     /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Categoria
     */
    public function setNombre($nombre);

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Categoria
     */
    public function setDescripcion($descripcion);

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion();

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Categoria
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Categoria
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt();
}

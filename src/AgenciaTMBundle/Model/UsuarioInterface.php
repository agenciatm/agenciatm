<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de Usuario
 */
interface UsuarioInterface {
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre);

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Usuario
     */
    public function setApellido($apellido);

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido();

    /**
     * Set dni
     *
     * @param string $dni
     * @return Usuario
     */
    public function setDni($dni);

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni();

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email);

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail();

    /**
     * Set domiciolio
     *
     * @param string $domicilio
     * @return Usuario
     */
    public function setDomicilio($domicilio);

    /**
     * Get domiciolio
     *
     * @return string 
     */
    public function getDomicilio();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Usuario
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt();

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Usuario
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt();

    public function getPassword();

    public function getSalt();

    public function setPassword($password);

    public function setSalt($salt);
    public function getRol();

    public function setRol($rol);
    
    public function getFoto();

    public function setFoto($foto);
}

<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de estado compra
 */
interface EstadoCompraInterface {
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EstadoCompra
     */
    public function setNombre($nombre);
    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();
}

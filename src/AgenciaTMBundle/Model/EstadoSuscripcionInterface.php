<?php

namespace AgenciaTMBundle\Model;

/**
 * Interface de estado suscripcion
 */
interface EstadoSuscripcionInterface {
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EstadoSuscripcion
     */
    public function setNombre($nombre);
    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre();
}
